import Vue from 'vue'
import axios from 'axios'

Vue.prototype.$axios = axios

const generatorApi = axios.create({
  baseURL: '/.netlify/functions/'
})

Vue.prototype.$generatorApi = generatorApi

export { axios, generatorApi }
