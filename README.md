<img src="public/cc.jpg" alt="LOGO" width="450">


# Crypto Criminals NFT Avatar Generator

A basic Netlify Functions-based avatar generator

Just run the app and press **Generate**!





## Features

- Works on any modern browser
- all pic generation is performed on the server
- Crim DNA is also randomly generated
- 12 different characteristics with up to 16 variations



## Web application demo:

<img src="public/cc-ex.jpg" alt="Demo picture" width="300"/>


https://crypto-crims.netlify.app/


## Installation

- clone the project

- use Netlify CLI for dev mode or build

https://docs.netlify.com/


#### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
netlify dev

```

###     


## Author

- [Leonid Anufriev](https://www.gitlab.com/mstrleon)


## Tech Stack

**Client:** Vue, Quasar

**Server:** Netlify Functions (Node)


