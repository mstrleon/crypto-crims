const templatesConfig = require('./configs/templates-config.js')

const helpers = {
  generateDNA () {
    const dna = {}

    for (const key in templatesConfig) {
      dna[key] = templatesConfig[key]
      dna[key].selectedVariant = getRandomInt(templatesConfig[key].variations).toString()
      dna[key].filename = dna[key].name + ' ' + dna[key].selectedVariant + '.png'
      dna[key].folder = dna[key].id + '-' + dna[key].name
    }

    return dna
  }
}

function getRandomInt (max) {
  return (Math.floor(Math.random() * max) + 1)
}

module.exports = helpers
