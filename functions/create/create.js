/* eslint-disable no-case-declarations */
// Avatar Generating - using Firabase as a storage for variants and final result
// (c) Leon Vasilyev

// Simple CORS
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Headers': 'Content-Type',
  'Access-Control-Allow-Methods': 'POST, PUT, DELETE'
}

// Firebase database and storage
const firebase = require('firebase/app')
// const firestore = require('firebase/firestore')
const storage = require('./lib/firebase-storage.js')
// site-specific config
const firebaseConfig = require('./configs/firebase-config')
// fake browser xhr for firebase-storage on nodejs
const XMLHttpRequest = require('xhr2')
const xhr = new XMLHttpRequest()

const helpers = require('./helpers.js')
const templatesConfig = require('./configs/templates-config.js')

const axios = require('axios')
const sharp = require('sharp')

const handler = async (event, context, callback) => {
  // CORS proccessing
  if (event.httpMethod === 'OPTIONS') {
    return callback(null, { statusCode: 200, headers, body: 'This was a preflight call!' })
  }

  // VALIDATE REQUEST START
  const errors = []
  if (!event.body) {
    errors.push('BODY should exist')
  }

  // FIREBASE INIT
  const app = !firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app()
  // const db = firebase.firestore()

  // Storage INIT
  const rootRef = firebase.storage().ref()
  const appRef = rootRef.child('avatars')

  let URLs = {}

  try {
    switch (event.httpMethod) {
      case 'GET':
        const refs = {}
        let folderRef = {}
        const dna = helpers.generateDNA()
        console.log(dna)

        for (const key in dna) {
          try {
            folderRef = appRef.child(dna[key].folder)
            refs[key] = folderRef.child(dna[key].filename)
          } catch (e) {
            console.error('problem allocating file  ' + dna[key].filename, e)
          }
        }

        // Array of pic urls
        let resp = []
        try {
          resp = await Promise.all(
            Object.values(refs).map(ref => ref.getDownloadURL())
          )
          console.log(resp)
        } catch (e) { // TODO then trying default pic
          console.error('No picture Found')
          console.error('getDownloadURL Error: ', e)
        }

        URLs = Object.assign({}, resp)

        return {
          statusCode: 200,
          headers: {
            'Content-Type': 'application/json'
          },
          // body: JSON.stringify(URLs)
          body: JSON.stringify(URLs)

        }
      case 'POST': // Here we process received urls and create avatar
        console.log('POST initiated')
        URLs = JSON.parse(event.body)
        console.log('POST URLs', event.body)

        const buffers = await Promise.all(
          Object.values(URLs).map(url => {
            return axios.get(url, { responseType: 'arraybuffer' })
          })
        )

        let avatar = 'iVBORw0KGgoAAAANSUhEUgAAAZAAAAEsCAYAAADtt+XCAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAMOtJREFUeNrsnQmUnNV15191V++LWtBit9UgIBY20BB5jO2AmiR4kO0EZE5iZzxzEDmZCU5mBmSfJLZPHKF4t48DihNiZ5OwZxI7DkFkwyE5xy08MeREQAMeCbO2xmIREqj3rl6qet7/1fdVf1Vdy/fV0t3q+v1wudTd1dVVX937/vfe9959xgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAArFViXILVxyffc/pAiIeNfO7B14e4WrAG7L3f3vWUepy190GuFgKCw7zn9D57p5uEYqP371BOVIDhwO2IvUlYhqzDDXO1YZXYe7938+3dv5XDiGfjvr1LWIaxdwRkLUdYEout3n3PMv55OdcBz+EGrZON8IlADW29x7Px/oC9LxfDnp0f8GydDB0BOWUd6UbPgW6sINKqBUOeqNxPSQCqZOsSiRsCwrFakKDsl6BYW9/PJ4WAnAqp+m2rUDSKlQL2e2KCg0HUAOkGz9Z7ToGX7IvJHkpdCMhqc6Yd9u7mZU7XayUm95CZQJFM4+ZTSDQKMejZ+T4+VQRkpYVj1ymSbUSN1vbY2z7mTOrexiUUO7zMei3a+W6EBAFBOGrHPs/JSPvry8b7PBvfUQdvFyFBQJYthd9bJ8KRL+3fTXmrLmxcwjFQh29fQnILNo6A1CIau9Oka7/1jpxrJ0sk15yN93s2PsDVcHOBO8m6EZBqONbtXkTWw9XIYp+htLVWgqN6KVVFYcSz77u4FAhIOY4lwbivhhGZv5u2FHod/avYybQs8g4s5pS0cX1ut63i4GjIs7FS9NfwPSjj3s5iEgQkimOpVLW3AqMc9Az/iRyhGKrUEAN9g4LCsnWFhUZZCLXjU8e+B8zKzuX5wnAg5+uK+7t5gV9/jrBc7t2XGwyOePbNXikEpKQBqg58ewRH0M3vxzO00pGKJzB9ZrGVRC2js1zu8tJ+orXVm1XvimDf1cqy/VY6wys9dxYQGInJRrPYoyuUfdvXvxNLQkAKGZaishtLZBYHVotYRBSVYG+iWkaew17KzyR7/WUdwwEfGTpVbCBHVEr179rvZSMESQhIxoDkVPfliUTWZN8o7/3KSfz+RbXIUHYzN7JqPu87vMyjFhmG8w+Tbl44vMYEt1B/ryEvSBqud9uK4VwuOv9eYBDd70VQ++vFQGrY+HHQMAG50pF1tReCDJs6a1DoBVxBH/HF89p6z7Rjde5gvnjICO7xRGOEa5JpCFmNzGTYUNJaqc/xvioFBH6ftD31/jl6oizfuNnLTOpaROpdQHastdS7BpnJzabyDZQagHbSKmJZ7frOKgQAfnNNVh8VzkwG6tmumQOBsI6ywxOTSiLanWzMqvlndbsnHpVkjMrG9xFYAQICtchKVOIaKPMpNDDdwpWsyWez15S/o3zQpEtUZBuAgEDNByt/rqScAQsRWT3isc+s8rmNc885K+xepqGXXn6VBRsIyNrCOoCi9T7vdnnAGQrtHh/M+fpAoLzgbtZRVkV5oYJeSojIyoqHhGPFe5lZ3/D9wheJrd6PKtkA6/uJv4FxaLX4CwICYSMmf9nfQA3/VLAlhC8sg6eQkCAiyy8eKyIcAaEY8IKoPrO8rXf8PV17EBMEZDWKxmo6uW3Yc5gnPKdZttS+DCFBRJZHPJZNODyxCHY/WM52OmGQT+xeqWALAYFch7nDrO6upssuKhHPl0BEaice+qxrdn6LFzgF24D0m1Pn6ANdm1vISBCQlRKOam7WWgn8tN7196qFoHirtu4McY0QkeqKx7AnHPtrYPfB8mz/KX453R4la/v7sCwEZDnFQw68d429LV9Q7rEOVdWINeTZE7ew2bDkdSxld1U/o8XLMiQaN5i1eyrnPmvzBDAIyLKIR5SW76dyZKbo9X7rWFWJYr35kb2meFlrO3sRimZz9xV5yKAnwsNVEg2J1c1rIMtARBAQMo9VICZ7qpGZlDiwiyZ1+a9ZbtPP3GtWlQOPPPtey5lGKTS5fgcWh4DUQjwGPCeuZxTd7vGitbLnTEqcv6K/cQVdfLOu1eMm/zxSxedTeKum/E2hPVxxcy0rtBCQaotHMSeuV/ZVmpUUyUbUGXk7l9hdo/vyCG3FWYcXEN1Wx9lGwSDJ2vT5XAYEpJoCorR2V62e/8orf3LJ9yYmxs0zzzxzKlyeQVPBuvoi51bUffPFAs0RdZ3LPmfFE45dprabXMuiq6vLXHTRxZ5PXFny8c8++4wZH58wjz32aLVfCqUsBKSq2ceL1UjvJRRyjIsvvtg5ytlnnx3q99Ji8mxGVF555RV3q4HjrKSQ5Bssr6jX+RBv3uPxaonqahKOhuaYaelpNB/++VucD5x99jmhBKOUmDz22GPOJw4cOEAWgoCsGgGpqEW2HORXfuW/mq1bt5rOzq6qvrZ7/+2b5hvf/TMzNTlnYsdiZu5kyky/Nn/KCkmeg5CGTR3Oh+SZ9xg2ZR7M5e1XCrupsya0nRE3zesbTau9b1wfMzOdc6ZlIW6uPG+L+ej7d5n2ls6q/j0FV9/+9l+af/iHf7AZyni5T3ML+0MQkGoIyIumjLkPCcfOnR8111yzteqv6fjYMfOVv99lnj7xtJkzSdO50GIaTUPm5wkrIhKSxLH5lRQUOd/OqJPteSbY77ID5846E5DgUvGyJsq9zHmXWYEl5xILiUbHeU1ONHJZsP9Nxmas5S6YM5tPN7tu+orZuGFT1V+HhOSuu36v3Ixkv7Vd5uEQkIrEI18ZoSTve9/7rXjsrHrGIY4cf97svvdj5tjs61YyYk48YiU+SgnK5NE5JyazJ5PLeQlHvGwkctnF23zozztpae9gPdicfd/KFPzVfrvL2RTo7RYvtFS66qgk1XGuFYsz4+5eX4dBIjJrZeSs5l5zyzW/Zq655D1Vf21zc3PmO9/5K/P7v78n8u9au2WMREAqEpDI5avrt20zH935MZNaWDBtbW2mpbm5quJxhxWPV2dPmGabc3RY8YjK/GTKiYkvKsvEoCmj71BglZaEaM2XsgKlqx5TxiorL+sotDy6qsQ7GlyGUSjLCIsvIt0LreZL/+nrVctEFqz/jYyOmvn5eROLxcwjD//AfOYzn476NNurtZF2LdLIJShOd1fnb9m7t0TJPD72sd8wiZkZk0qlzOzsrGlvb6/Ka5mamTC/9Re3OvEIm3kUihZbe+Omc2OzWfcTLaZpXaN7lrmxVC0vZZ+97bDXc2Z8fOKRsL/0/eenn756U/s/2X9+yN7a7NdrOgux7/XjJr3ze1vUjMubJH/Y1HDnuESj64Jms+Ht7ea0/jbTfnaT+14lNJu4mY8lzXRszhx85v+Yd198remowpzIjPW9RCKR+Xrz5kvs19Pmhz/8YZSn+ZG117rIfMlAapOBhJ7/0JzHfffdb06OjLiox6dn3TrT1NRU8Wv5zL2/YR5+6RGTMgtm3UKbE5HIH3hDzDQ2Npj5uaVlrNTsgpl8ac5M2aykxpnJPhNxbsSLzFXW2b5Wz+r2Wr1oAcG1Zcx33GFqtMzcL08p2NDcRt5I1NpUW3uLHaBn89pWyWzB/jcSm3Y2/c5zrzK/fdOXK37d4xMTWQLS2tpqujo7zfbtN7i5kbCZs7XTaxkJC9gGlyBU5ByKT30q7b9B8RCqwVbKA4/fZx596aBN9FOubJVPPJSml2LDhnXm9N5uE29qzDtQdJ3fbM68usP03WQfd2VbwQGjQnZIDLwd0KHwBtRrzdreyNkXVTxUsrK3vbUQD5WmNlzV7mxB98VsQeLR0WkH6O7S2Xa8MW4aYg05kWzM2bXsW3Yue6+UXL/z/VIrIiPAznwykLKzjwETsnWJ9nV84xv/yxmt6q5BlH0oCymXyZkJ8+t7P+xKV02m0XQttC5xyMaGRpNaSJm5+eJi1XvGOhctvn5iLHSkqEn30R/NuOxEWUoVcaJQ7c6/dWSfflZWtZKVsuhE70kXSEQpTSkgkXhMTSbMTKK4DbY0peftVOKdS2Y/djyWMPNWRHpa1pnPfWCPOX/DhWW/l+MnTiwNoHp73X2ULISJdDKQmvPBD36o4M/kKOVybOwV85t/89/diivnfAvZpbCmeJNp72gxjfEGN2mYy7yN6YK3E6+NmteOjUQqMyjy7L2qzVz9y1fYLOt3Qm9+DBndfc9b6QYrKB7a4PqlL33ZfO6rnzZdb4s+ryF7ev31MTOamHKT4hIC3TSvMesW6y7aZjKVNB3dLaarRwtMWrIyZxsKpctZM6Npux97paz3UyjrT3q+qD1ZgIDUmp4oDqgU+Y033rBR2KSZnZnJDOjJZPnLZv/l8ANm+PjzzqmU5se9j0xOJ+fr7mk3bR02WmxqcI7pi8aEdd43YpNmzN4Hb/re+MK0e8ySdLQhZpqb4+5+iePZqPDZ0R+ZCy5/s5vnufvur+Vtv4KInFrioc9Qn+Xdd/+R26/0wNDfWAGYzfvYzq4207O+c4l9yDYkFiOxKTNlf3fGWpf2Juk2beVjwv1s2omJ7FgBVSq5YJqabdayrtVmJM2moaHBC5Dizs71uAmbeQ+feL7i6yVfnBgbM+Ojo+alH//YbSy85pqBqNcbEJDIhHJQReSnn3aaOWqNc3JiwiStkMwkEk5IciOfqDx06EHnoM65nHyknbc53mzaO9PCIWZmrLsupMzY3LQ5+fSkmX4+HYEtTFiHPbZ4cw5ln1FiImcPRoYqQaw/vct0dLQueR3apJiwA4AGmPTAc6UbdKokJIhINCoWj6Bw+C1EtDn1X59/qODijCYbXLS0Npm2tsVl6RKLUSsOEgtrWCb5fMrMDc6b2Qfn3b2+djY3OGdGH5wyb7w2aeYW7M9n0/apzLm9q8WVYNMD0uK6QtnmvY98s6IMxPdFP5hTkHf8tdfMZZddVvVxoB6JcwkqR318JqemlpSqUjbzkAE3xuPu340N0fRa4vHK+MuLGYLnWn7Kr7mMtJOk3JLFkZEpM/NgOnpsGmgycz9YdODMc3TGTNMWKwdvsoJg3X4ull5/r+f2ny8fMe8/DTC32sjQbz3hC4l6EP3pn/5JJb25fBFhTqR4NLy3kgFNc3W33/7RvH2nDj7/A2sP866MVKhMpQw1mUxlxEPlKjfYn1ywIjHvApas7OTHKZM8bDPnyxudPc48mDIjA1PO/lqtEMmW/eedT9nfTy24MGnWy5CfOfEjc/jok2bzeZeV9X4LZf8zNitR48YK2pwAGUh1kDMmpqer/rwPHf5nO8QvCkDTQvrj6jmt0/T0tpu52aSZGJ0xY1Y4xmanM+LRuKkxr3j4GclsIDJUduMPAmMjk2Z8bMpMTiYKRBsNboDRQJPvGkhINEcix6xARPZSMigoHtrUuqOc39VnorY6WuhRqGnhgcP/5AbuxoX8w4Js49grJ90kuUqgvt0o88gnHplAyhOX2HqbX9jkZfbgvJmw4YvsdmI0YVrbm0znula3OtDZ2cKigCmzOXD4wbKvWaH5R+3P8rv+AgJSK0IvpyxUoip3/kPlhCePPpZVYlJkqEhNN1fSaou76G1iftqtlFrwStfzh5MFnTkTTR5cdHgNGoomFVlOTc64KDCY6SwaS4MTtIMv/KDg82oj5X337a9kktJv/AfZ4lH2dVG5SsJRbKGHeO74s0sGBZWW/PJSkAlfPGRLT5e2NzeYH0s5G411xJwwzKTm3Oqt3Iy6KZABKcD59+f/NbpweCWrQgIyH2FpPYdLISDlErqUoom6ogP2fLSGhoePPmGj/UXj74i35N27oYlzVaAbzoyZ+ObGjIhkZR3Wmfxb5nv2cakfLz7/dM7EqSY1Nc8SHDxiC+m69FNHi18W9f/64he/7Fb1lJmN7PB6OcEiZR2jrKxDmWGplXMqEyU9ews25ZQd5O7bmHELbRdtKfXqQpatKZiatzfd51sZ2Lgp/Vw2ZzZz1i+Cj9EE/enru82Gzu7M90Znxtzri0Ipf4vqj4CArCj5HKkYh156MrNSqrWpyby5d0Mmxc9EgWMJM5ucy0yyJ3+cWvI3520GNO85tG6pwOvQpLpfytKAMBtYmRVvSGc5wYHDXwE2PjPu2qqUQqt6FPmq7l7OgEkpK5N93GEizntIuDVJXirr8NFKv6T3+Qfb4/iffzCQUBnT2Yy1t/knky6zcPamgdmzMdme7ufziIgfuCibVSlpfCSRsdemeKObqF/fsdjKRMKm17ccfgcIyKqi3BLWa2OvZoRhTiKg25xunljMW+ebmXexYMZZcicwF4IFMD9jWXRo1aaDcyXJQMbjlxUa8kz863FHjr8Q6n0o8k2v9Im8UkvicXu9248nordFF48/inRA09TsxBJbyfrsA9VM3+ZkN/NPJEsO1rnf9e10wftPtpy2aWuP80kzOZEwb4yMBzKeOXNsvLz9IKki/qfD2cJoK6MYAlIWYWufOgmt2hHQk0cfz5QJUqkF8/prY273uAZ2F92l0lGeP+j7S3RDRWW5jjzpLXE0+Z0tTIuUUiUtDWiaH4nIrijtTtYou0yE/Ui+eFQyQeyHHcHsM2jHGbs8uVCWrQd/T0GSOigoGNItthAzE+PTJplIZmVCR4+/GF08SiydD3lcNAKCgFREyYl0nclcMAPxaq1R94E0ByYSWwKrrTWYj72RcKtXNP8R83wxlqdjfEOBgT+MHOi5/QlOv3wRnJOZDFHCykXno5RRztpVr4bniWekLKxc8dAcgy8Mfuarz725Ne5WSc0nl84ZaDI8TKCRa4daSm68KTfNt8jWpsZnzWxiPvMcWZtmI3ZckpBpH8hCEZ+L0EyRJeUISEWUNKBSex8UCSUCm5nCEMwGcmvSeh45mm6+kwVLCUHHzXXeBu/3sr633itXBf5OS5uWVraY9q6lyqRa+ZEydgj7mUjEifUddZyF7IjyYDUJrCTz8IUjFRiI2zqanIB0dLRlHudPsvuT4X7w0uCCjYaMberrpsalCz/8peT+3qLg/ErekqmJFnxNe0vqi5WPX3nl5bBPd4QhEAGpqYCI48ePFxYQz5CnK9wrIkeLBQTALbH0PsLccsLi7zQ4J45798HNjNrcpUFAm7rSxrA4YdrS0uRlUKmMg/uipr/U3lzeeQ0SEe0VqeVAuoYIPfehzC5il9kservPzGtrmpfwgxjfDhq9QEO20/SuuIlviWcFKLI13YptnG18UyyTZfvPmxadhrwiEoWpEAJy6PBhMhAEZFl4IlQJ4PChwpGdZ8gy7EpWhfiO5j+Hc3KTdkQ5cjFysw6JR/yy7N/zNyrKgRNTc+6murSf+QQbOfZVcGqcVmdFnFS/uQ7LV1rGHHruoxLxEBs8AZE9NZmGjL1MTcw4O9DiDf97wY1+EpHGN6WFxJWmwgw6Z9rf2dzo+l4Fs4+MXccavUxo0VfaWzpCPXci2IOuyFLdw4cOISAIyOrJQIpFNP6acxl22CzkzRsuyCoZ+SKgBnQqKQSzkFY7sLt9IJc3LilNyVn9m36uNiYt25sypYRgmcxvYeGWU3p/J7j3pJrG8qEPfSjKw/vqsE9W6PerlW4S5UrwM0q/aadPV0+rswP1rPInpVtyOiAln0+6pbmyK92a35O+uezE2pxsL2NnVmSaB+Jus6DszRcQ2Zu6K2jOxf/zwdJV2GNup6amMll/sWDt0OFQAjIU5dAzBASW4PVlKmlExTIQGbPvfGGzkNO6z1iMzALf19yEHLqhMZbJDOSIrdYllVEEnVWrqxrOSjts83vi7ufppbtJN2eSfGHRQfX7WYOCt7RSjp0RQs+hNVdSbm8in3f/1NU1G1DXCKEV4a1vu7TijXHKKP0d4MH9QLOJpJmZnk+3/48tBhtBEZl/OuX2IM18e9bMH0y6YEY39cdKHl7cJ6K5EtlirDl9eFQwM/Y7/OrLfP6xsbe0gEg8/Gy/2PU4ePBg2MsyyAiIgFSDkoakOZAjRwrPtyUjZiGXnnfFogCZxbYM/sCecTLPqdsWmtx8iBzUFxHtNpdQaMIyIyoTZtGhmxYHhNaF7JVe6rM1dnLaTI7NZCJTnVstzu+9sKKLqdd+8uRJc+mlkUSoDzPMz9ve9jZ3iFkl5VEFBH7eoY2C/tEA05Oz7uYHK5mMZaE5k6k0vmnR3oJZsDITvzOCbFJZScxmxV15TtT0u0qrzbtr956ze6RUBqL3PhXwq2KngB58NLSAHMC6EJBqEMqQHnqo8MOCBh0mCwk6jFsrrwE8pc6+sUxpQWWFYAlKXXUbmxtctiFnVWsTOXd88+Ljmt7V6MoIrg69Ke3scuhMp18bCba2NmWVybROX63i/cj0svOurOhiqgOq3v+ll14a5deGsLn8nHHGme56VtpZ9qoLrs5kIPrMdZMN6PCndae3ma51bZmSpm9vrvS5JV0addnF+kVhaH5fupSVLmvZwGZ9g/sdv1Ta4K3Y0vM3t8Q9+0vbm81lsl7bhpxJ/lymAz6VPm8kWSQD+XcyEARkWdkf5kHFIhtlIH4ZS4Y+ETgrJB+bei/K+loO5da3zy/Oh2iZrc6hbm5Kt8X2ndqdG3Km59jKSN6UXYP2HVqlhM6AQ7s+RL3d7nyGdIfUFu9vzrmzQPyB42c2X1/2hZSQqvV8GVCLLoGu61yEJoG5vGvTNZls0x0KZT/3eHN63k32pUH+tNO6MuUmZbwuaDHpSfGgnfm2Jjt0RwjYR61baMvYmvpraWmwhClYJp1OpDNe397c67rgmqKvWxPnk97ch7OxIval8tVU4LHFAhbmPxCQqmANadiE2JGqMlax+mqwA6j2hZwcGSmYiXS0dNpIf7GM5TtUYmomq9SkjrytbU2Z3lUa4FVflmM3FzjXQUhk5NDBx6xb15F1JohbxrmQjgRnvIjwTV3nhZ7QzJt9TExgUDVkMtzgmJeBzf/RrG/pybK3uZy5BImHRMRH4iE7ks01m8asCXj9THNr+nmXtcdg2cptUmzJnox3beLnkkuaNV53ybaCr1mBWG7mVUxAvvvdB8JejnuwJgRk2bOQYgYaPObWCYp1TtWuC0343Xr1baarJe2sKS8qnJ2ddy3Xc1myTNe6r7KL9QvtTkz0727vpu915NShdQqhmthlvd7ZdIt3/yhSDQ4ffscvl30BFSlWcLxvvZWwyiqfVJDhOT7Q/8GMvelz16Cea28qY+n0yng8u1uCb2+nLXS4m4RDcyWNOcNMZl/RXDKQoafM5Hi6qaI6Qzd4y9O3XnCteWeeDER+NDY2tmQ+UeJRKChTgBdy9VVof0dAoKoRiQy00KZCVw7KcW5fRPI5/QUbLjIf6v+wc0w3oHtdUDWpqZUxaSdMuZUyrsQQb14iJP7y3GZv2WQ8J0psb28xvRvWmc7utqzfm56aNSdfH3eiNe31nVD2sfWS95R9AXNLBy+88ELo363DckLo9/vDp7JbnU+WKI8W4339H3Cfs7MB+7lLRNwhYxPZh4y1d7SY0zd0m26btYbd+Oc6JzTGTVtbi5u/0/4S2bLudTDafHJxo6oE7PTm08xt1318aSZfxGeKHatw79/cGzpY8aoOgIBUrYw1FDYKLmaouVlIMJrKV5vd1r/dnN2dPstBib0m1NWXSBu8Rk6kT3TT7+tEt66eNtPS1Gya4k3ulu8goKxSxOldpmtde9ZkvNDJhGOjk1lHlioivO26T1QkHrnZx2uvHatpNL4G7C0UuUKs6zxVZilLRxX/t2v+ZyY7lYhM2YxADQ5HTy4Vprb2ZmdHnV1tmYxEB57pa90kFh3tbfbW7gKcNhuwaNK8oSHdFFSBkDKc2flZN3k+Hku4bFelr198x82Zo5ODWWyhrF2+VaiBooK6YotcctjDiIeArFgWIkONkoVkIkfr9LnLMeVAv3bdbxrNVsixxqyDyaHH5ifdJKdu7lApG8lpKaQmwDWxvr63w5y2ocuccVaPOfPs9e6mCfJuKxiKHM84s8c0NceXvDYt252ens066lYZy/s2/7x563mXl3XR5OxTOaUGRckRMpB6jQZDichTTz2Z15bK3Rvy9k3vNr/Q/0uZTDVh0iVMdSXQ2R3+kQKZkpQNQGRzp/V2OTtbbwVFX+vW3dPughu34MMKR3tnsytXaZn4XHLOzMzNmPH5KfPGwoQ5GZtyJxXq7159wVaz7YrtWbbpz3fkK1Hpe1XKPihfISA1Y1/YBxYz2JlEouAyQ9WwX3/jjaz0XGv0//M7ftnVkuXIcmiJyEkzaeYXki4jmZubd46tyE4T666vUEN240TVrhUBBmvXQq3hVUZQl18NEvobEwHxuLT3reaWa36trAvmlpdOTCxx+kceeTjK0zxRp/YWSkAkxk899dSS748VGGzDcMs1v24u631bJhNR4KKl3O4I5dGEKzk5WwmeJligG69/GqZWW01PzJrxselMxqEgRbYcnDR/a+9m85HrfmNJyarY/qlEkaXxEbOPfay+Ck8jlyA84+MTie6uzj4TYle0NhVu2fJ209OTv52RygzNzc2FRcYrdTU1pfdjXGKj/5GxE+aZEz9adExNrMfmnbDEJAI2c5idSZ8zrclPCUWpszyUuaiVtsRHDj1nxWgsNp1pIyHx2H3T7y0pJYSlULnhL//if5ujR4+GfZov2mtfd1mItbX19i7c0b72Y77qqncujcptQNLa0lLWmS7vvnjAHDryuDk29ZqztblY0jQseE03F4zLIkRwGe6ifWvTa9I1V5wYTzjbTCSsrU0lnO3LvsZjMy7jCHJx70+Yz960J2NvEg0JYbGzPZTRF8s+vv7HXzMvvxy6++7OerQ1BGT5nHrUhOwO+4o12kI9ivyoLN7UVLT0owGgKR53E5X/YdO7zRtjx82zJ7IPwtHk+oIdHxr0fymTERI5rIREE6CKHONNcZeVLD5/0kzZiNDtcLdOPZ2y0aEVDz8a3GwjwU/fdGfZ4qHMYzZPuU5zH3f/4R+Efh4bEd5Sp7amSDjUeSAvvvCCueGGG5cEJfpsyxWRpnizFZFrzQuv/si8PP5yZn+Isl6bjsiI3TLfmDo9exsMJRwTY1NmbHTKlUInJxPp7Nja2sz8nA14bMYcU+Yxu2S3+aYNF5kv3fRVZ286P0fzglruXgxl8okimYlaDH3rW98K+5YHra3tZpRDQGqZhQxbxx4wIVprHD9x3GzYsMFs3NhX0PglDI2NhT8G/ywRub6ykXds+inT1dJtDh55JFtsrDsqG9GqmVnrpKmYd2phUkLlicXkjP2bKfvvlNtPIid38ycLmiidcWv/fZfuP/cnze/e+OWyxMOvVxdy/j/5kz92A15Yp7bX/J46tbURa2sKVkJ15ZV45GsPU6mI/PQl15vEzJQ59OoP0yJhbU224rJU+7+5xLwLUqasTU0pWLG25o6qdTaZdI9VqUr2qSMB8p18qc2Cu97/Bbf/yc86Si35lv9MWTsrVqb7zGc/HWVBwW57zem+i4AsSxYSqp2slvX+7M9e5wb/QllGKRFxabodABTNKxu55JxL3YmAT7/6f/MP4Gp7Yp1XmUnCExQdfZu0orIwZ6PI2Tkz7e0uVySox/nRoPadfHDLfzEfve6TbvAoRzxUtpotsFBA2cddd94Z5Sn3WKd+pI5tTdHHVWEeq0UJ2977vrylUYmI5tVkh+Wct3HlxneYK87dYg69NGTGZsYzQjLjbMxKgmxME+3235rTcHtIPMFImdLzMJ+54Sumu219qKzDfz/TVhiKiYfmISM0Thyu10wXAVn+yPDpsJGhBv6XX3nZvPOd7ypaqgojIsFspKOt0/zz4X8MN6h70aCcWaKRFpX5rHbZmizdtvnnzMev322u2nR1WddF72PUDgDFIseI2Yf4iCLxOhaQtrDBimzt5MjJJXMhQXHX3FqjDULijdFd/4zus8zP9/+COa/7PDN84rmMkKTFZMHZVzi5yOa6ze81V2/6GSceYTaahsk8NAf51T/4/SgvYyfZR3RiXILy8A78uS/s42/91VtLntnQ0tpqmltaQj1f3A4CL44+a7716D3myaOPL/m51tFrtVYpzuk+17zrgqvNDf2/WLJhXTG0Pn8iz2qrIFop9MlP/FaUp9WGriuwtbNeNBG6EX/2c18wl11WvNNxW1t6b0Y5k+uZkP34c+ZfDj9gfvDC983LYy8ViVIbTOdCi2lft94dDPX88Wddm54zu842P/uWbeb8dReG3j2vnnKlMg+VrD7xyY8XPSU0T/ZxPqMaArLcjv09ezcQ5rHt1lk/9du/YzZu3Fj0cU3NzU5Iwjp2Z0eHSTUkzfMnnl0czBNT5scnXjBDRx+1EpIyqVj2Cpazu85xE5aXnXuF6dtQeWt2rc0vNQAoarzttv9hhoeHI0WF1rHvws7OUs3v9rCP7+vrM3v2fNU0lMgyFIR0dXa6+0qZmpkwz1kbfG3sVfPq2CuZrLa7pds1BtWRubkBimxmPMJSY620mglR3vra178WZdmu2G7tjL0fCMiyO7bE43thHy/xkIhITIohx2+zjwlbq1Zdu6urq+gZ1LUg7ACgn3/zG/eY73znr6I8vcpW57Mm39mZso8Xo/zOtm3vNbfe+pGSIlKtbKQWQUfw8RKOuRCPf+C7D5hvfvMbUV6OVl5dy2hWHsyBVIC3Ikt7Qt4S5vGjo6NugnnLli0lHUadeyUgYQYAf25Ej61GNFkKzXVolUyYg7H0Xh49+O/m7rv/MOqf+ZZ17G9jZZnVWKHtTDz33LPmoosucsfdxkoEFvo8p5fJfjRPo3myuSK75PUa/H0fylxVskqG2FWvJbtf/YOvRn1J2+31fRUrQ0BWBOvY/2bS+0JawzzenVqojYGbLyk9UHvt332nVqahFTaFWlRo5ZMcr6W5uSbvVWvztetZcx3FNnZlhM06/+snjpvPf/5z5fRm2l7Pk+d57OyYCbn/yEcHJ23dOmDaWltDZbOyH02yS3BqIST+jvJ8Gauyn1b7OmW7/go+ZRxuh3kIW5NffeGLX4h6HspdNkihbTsCsuLRoVbKDIT9HUVKxfaHZA3a2rWbTLoNhxq0Vabq9spV+jrXGeWkHSVKZOU4vnorqewQtr+SP9n5hc9/vuhRvwXYh2PnzXZlY31hf0eD6RNPDJmt12x1A3RjCFFwu9c9IfGDl2qVtmTHiZwd43r+jo4ONxejn/vnmUg4iu0uzxWPT3/md6MGKSNekJLAuhCQlXbuQevcWpV1Vujo8NGDoUVEEZiiMZfaq7xlB+d21a2t4/kO7u9s96O4auBWVtmMI2pjPjm+BgBNZkY4f5rso3QWciRqFqKy6fETJ8wV/f2ZACQWIhvx26CoCaafbZaz9DdrsGlsdJOuam+irKjTBkKyY31fZa2E175HS3STIe1NovF7d37FnLDvMSK/FKXjMSAgtXZulbJujVRiiCAifkrv9opYB3SlhkDa70+EVioemtjU3Iab5CzSHjsfKllNTkw4sdFk5t/93d+W8xLIPopnITKW/ii/pwhdInLlFVdmApEomwn1ecoWZBdJzx4avfPMo6IybEtLS3pDo+zYvh6Vtfz9HxKPVMhDx7RM94tf+kI5Ge5+WpYgIKvNuV+1zi2PGqiliMi5/L0is97udEWGjWVEhwteNjPjnSmt3lX6dzltwCU0/uYuLaH88z//s3Iuo7KObZQVigYqB7xApTXK7/kiogUc+nxd5lrGqj3fXpSZyPb0tT/gN4QUFT+70VxacCGG5vzmQq7M0vv5nV2fKifzGMbGEJDVKiKDUevUvoiEnVjPdOj1nN+twNL6eHtTzVuRnC8A+pl/06oXV9u2Nzm/bpoQT3i/FyXTyCdqvniobBXx7IUgH7GR4SNYUlEbU0doTQ5cH/V3Nege+X9HzOWXXe5KSW5epIKylGzGb/iZ8ERFJSV/DiWR56YgRY/Ll93qucKuttKEeZmHZmnPx9NYEgKymiPEHVEjRDmFIsRLLrmkYN+sYBkgtwShwVvi4XpmeQ4dvMmp9TMXMeaZfK9UPCRGah3x8MMPl/tUKit8AgsKJSKPRF3W66O25k88+YS58MKL3CbUqOWssMJS6FYMt2CkhICoNKqluhFXW/loY+q3sCAEZDU790hgaW/kCFHOrQhRE+SFaGlrW7ZNX2HEQ7vL1fX0ueeeK/epVLq6lrJCpEDln0y6R1ZP1N/VxPrDD//AdT04v6+v7HJW1e3Jy2jyoWxDAcp3rYBUEKDsxHIQkFNBRIa9jr3Xl+PcmkOQc19ko8RCkZrbZLiCTu+Lx1/f+9fOscs9g9tjG2WFyDaWCMyHREYR/JM2WDl86JC5cNOFprOzs6JyViVo3qPYTnNl55osryBA0WorluzWAFqZ1JBzzzlrbzmZiI/mRH71V291k+z5kIBoQj3unVq4XGiZ7tDjj7n5jggN6wpxixWPfVhL2TYm+9pbyXOotY5an/zc+3/OnNbbuyy2lPKWps8XmX9TUPK1r/9RlJbshbJbWuIgIKesg6tj742VOve267cV7aElEVEpopZiIkd//LFHzV99+9vunJMqsI8zGKpiY2q0eGelzyP7eq+1tRu3f8B0dXe7/Udqb1Ite0p5c3TBlVuFhENzHQ888I+VZrauNMp+DwTkVHZu1ajVcLG/UucOIyTCnfdgbypJlDsApLzVXCqX6d+PPf64+fu/+9tqCQfiscqy3Vxb27Ll7c7W1AA0aE+xkKVT2Yw/ce4LRqmFG8pmH/r+Q9UQDsQDAUFEwjh3GBq83ceNATHRfUNOO5TMahnP2TWpr/kYLTOuQqkK8TiFRMRHNqZzbFRODdpbQ4Ed7cky9hCpRPXQ9w9UWqpCPBAQRCQKmhvZ8pNb3OawzSH2kJRCUd+hQ4fcpGUNRAPxWB47k4DsrcVzy95kZ5ds3uw2voYNYHKRfR06fNgGKMPVFA3EAwGpCxGRc99Yi+eXQ8ux3c72N280HR3pUldv74bMRPzhQAlKAqG9J3Jkt1O5NoIRZLd17DuwhGUREc2J9NT6b8muNlj7are2VqibguxranLKDOu+8tIU4oGAUGqodqlhlSPH3slqq2W1MWW6WsDRVydvecgTD1ZbLSPsA1kBxscn7i93n8gp6thqH/FdPvlltTH1ZlNTyreYMnasn2IoMPklxIMMpN6ixAEvSuxZo29R55nvxrFX3M5UMt27Bu2MzBYBqXvnrum8yAoxbNIbBAf5hFeVnWleZMcaeUuDno0N8+kiIDj42ogSFRHuYaJ81We9u0zEYwdWmY0pq72LTxMBgaVRopz79lNROEz6jGnKVQhJrdhn0iUrbAwBgSLO3ec592ovNyAca0NIbl7ltrbPyzqG+cQQEIguJCpvrabS1qC93cPk5ZqztRs9MelfBS9JYqFVZPsQDgQEKnPuHs+5bzArN9k+5Dn0fhy6bsRE9jawzKKh4OR+a2P7+SQQEKidmGz1nLuvxs58wBMNSlT1a2/9nq1t9f7dU0UbG/JsbJAd5AgIrEy02Oc5+Ebv330RhEVOO+LdH/HuhxAMCCEqQTu7vIiwDHu2lbE3lngjIHDqObzBcQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAijVwCgNXHueec1d/d1dk6Pj4xwtUABAQAwgjHDiscd9p/tr308qv7uSKwmolxCWC1R+L27mZ7u8cOqENr9D0OeO/xRu9btyAegIAAVG+Avc/eVM7ZvxbExHtPN3ii0ed9e9Dettv3RtkKEBCAKg64fZ6I9HvfGvYG3Pt1v9oHXe/1SzS2eqLRE/ixXvtu+x7u4pMGBASgdgPxHfZuV54fDXmC8oT+vdIZild66/cEYyCQZeSijGqnfb3DfLqAgAAsz+C8N5CNFGLQE5ZR798j1RYWrxRlPJHY6AnFQIhfHfKEY5BPFBAQgOUXktu9bKSnjF8fzBnMR0s8fl1AsHpCiFchlGmoXLWPTxAQEICVFREN5hKS28oUkuVCgrWH1VWAgACsXiHRcti+VfKy/FVje9bqEmRAQADWmpjcaLL3VayEaNxPtgEICMCpnZVIRLTfYsDUrsQ1aG8HTHo58SBXHhAQgLUnKP7S2su9+/6IoqLsYsi7HTHp5cIIBiAgAHUsLH2m+LzJMPs0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA6on/L8AAhoTzYdnQ90sAAAAASUVORK5CYII='
        console.log('START MERGE')
        let merged

        try {
          try {
            merged = await sharp({ create: { width: 1200, height: 1200, channels: 4, background: { r: 0, g: 0, b: 0, alpha: 0 } } })
              .png()
              .toBuffer()
          } catch (e) {
            console.error('SHARP image creation failed', e)
          }

          for (let i = Object.values(buffers).length - 1; i >= 0; i--) {
            try {
              merged = await sharp(merged)
                .composite([{ input: buffers[i].data, top: 0, left: 0 }])
                .png()
                .toBuffer()
            } catch (e) {
              console.error('Error on merging i, buffers[i]', i, buffers[i], e)
            }
          }
        } catch (e) {
          console.error('Merging Failed', e)
        }
        try {
          avatar = Buffer.from(merged, 'binary').toString('base64')
        } catch (e) {
          console.error('Coding base64 Failed', e)
        }

        return {
          statusCode: 200,
          headers: {
            'Content-Type': 'data:image/png;base64'
          },
          body: avatar
        }
      default:
        return {
          statusCode: 500,
          body:
            JSON.stringify('only GET or POST')
        }
    }
  } catch (error) {
    return {
      statusCode: 500,
      body: error.toString()
    }
  }
}

module.exports = { handler }
