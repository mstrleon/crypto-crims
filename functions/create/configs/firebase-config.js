
// Stored in Netlify
const { API_KEY, APP_ID } = process.env

const firebaseConfig = {
  apiKey: API_KEY,
  authDomain: 'crypto-crims.firebaseapp.com',
  projectId: 'crypto-crims',
  storageBucket: 'crypto-crims.appspot.com',
  messagingSenderId: '136805604489',
  appId: APP_ID,
  measurementId: 'G-65DMHR2MZ7'
}

module.exports = firebaseConfig
