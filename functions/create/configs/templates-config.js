const templatesConfig = {
  1: {
    name: 'glasses',
    variations: 6,
    id: 1
  },
  2: {
    name: 'jewelry',
    variations: 6,
    id: 2
  },
  3: {
    name: 'shirt',
    variations: 7,
    id: 3
  },
  4: {
    name: 'beard',
    variations: 8,
    id: 4
  },
  5: {
    name: 'hair',
    variations: 12,
    id: 5
  },
  6: {
    name: 'eyebrow',
    variations: 8,
    id: 6
  },
  7: {
    name: 'eye',
    variations: 12,
    id: 7
  },
  8: {
    name: 'tattoo',
    variations: 12,
    id: 8
  },
  9: {
    name: 'face',
    variations: 16,
    id: 9
  },
  10: {
    name: 'chest',
    variations: 5,
    id: 10
  },
  11: {
    name: 'body',
    variations: 1,
    id: 11
  },
  12: {
    name: 'background',
    variations: 1,
    id: 12
  }
}

module.exports = templatesConfig
